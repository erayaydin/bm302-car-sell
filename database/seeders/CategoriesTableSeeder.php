<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            "Bisiklet", "Motorsiklet", "Binek Otomobil", "Minibüs", "Otobüs", "Karavan", "Traktör", "Biçerdöver",
            "Kamyonet", "Kamyon", "Tır", "İş Makineleri",
        ];

        foreach ($categories as $category) {
            $model = new Category();
            $model->name = $category;
            $model->slug = Str::slug($category);
            $model->save();
        }
    }
}
