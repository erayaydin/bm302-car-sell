@extends('layout.page')

@section('page')
    <main role="main" class="container mb-5 mt-5">
        <div class="row justify-content-center">
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header bg-primary text-white text-center">
                        '{{ $address->name }}' Adresini Güncelle
                    </div>
                    <div class="card-body">
                        @if ($errors->count())
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $error)
                                    <p class="mb-0">{{ $error }}</p>
                                @endforeach
                            </div>
                        @endif
                        <form method="post" action="{{ route('address.update', $address->id) }}">
                            <input type="hidden" name="_method" value="PUT" />
                            {!! csrf_field() !!}
                            <div class="form-group">
                                <label for="name">Adres Başlığı</label>
                                <input type="text" name="name" id="name" value="{{ old('name') ?: $address->name }}" placeholder="Adres Başlığı" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="address">Adres</label>
                                <textarea name="address" id="address" class="form-control" rows="5" placeholder="Adres metnini giriniz...">{{ old('address') ?: $address->address }}</textarea>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary">Adresi Güncelle</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
