@extends('layout.page')

@section('page')
    <main role="main" class="container mb-5 mt-5">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <p class="text-right">
                    <a href="{{ route('address.create') }}" class="btn btn-success">Yeni Adres Ekle</a>
                </p>
                <div class="card">
                    <div class="card-header bg-primary text-white text-center">
                        Adreslerim
                    </div>
                    <div class="card-body">
                        @if ($addresses->count() > 0)
                            <table class="table table-hover table-striped table-bordered mb-0">
                                <thead>
                                <tr>
                                    <th>Başlık</th>
                                    <th>Adres</th>
                                    <th>İşlemler</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($addresses as $address)
                                    <tr>
                                        <td>{{ $address->name }}</td>
                                        <td>{{ $address->address }}</td>
                                        <td>
                                            <a href="{{ route('address.edit', ['address' => $address->id]) }}" class="btn btn-sm btn-warning">Düzenle</a>
                                            <form style="display: inline-block;" action="{{ route('address.destroy', ['address' => $address->id]) }}" method="POST">
                                                <input type="hidden" name="_method" value="DELETE" />
                                                {!! csrf_field() !!}
                                                <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Silmek istediğinizden emin misiniz?');">Sil</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                            {!! $addresses->links() !!}
                        @else
                            <div class="alert alert-warning mb-0">
                                <p class="m-0">Henüz adres eklemediniz!</p>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
