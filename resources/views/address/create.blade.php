@extends('layout.page')

@push('script.page')
    <script>
        $('#location-picker').locationpicker();
    </script>
@endpush

@section('page')
    <main role="main" class="container mb-5 mt-5">
        <div class="row justify-content-center">
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header bg-success text-white text-center">
                        Yeni Adres Ekle
                    </div>
                    <div class="card-body">
                        @if ($errors->count())
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $error)
                                    <p class="mb-0">{{ $error }}</p>
                                @endforeach
                            </div>
                        @endif
                        <form method="post" action="{{ route('address.store') }}">
                            {!! csrf_field() !!}
                            <div class="form-group">
                                <label for="name">Adres Başlığı</label>
                                <input type="text" name="name" id="name" value="{{ old('name') }}" placeholder="Adres Başlığı" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="address">Adres</label>
                                <textarea name="address" id="address" class="form-control" rows="5" placeholder="Adres metnini giriniz...">{{ old('address') }}</textarea>
                            </div>
                            <!--
                            <div id="location-picker" style="width: 500px; height: 400px;"></div>
                            -->
                            <div class="text-center">
                                <button type="submit" class="btn btn-success">Adresi Ekle</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
