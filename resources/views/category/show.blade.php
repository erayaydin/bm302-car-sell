@extends('layout.page')

@section('page')
    <main role="main" class="container">
        @include('partial.publications', ['publications' => $publications])
    </main>
@endsection
