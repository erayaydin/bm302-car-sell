@extends('layout.page')

@section('page')
    <main role="main" class="container mb-4">
        <div class="row">
            <div class="col-md-4">
                <div class="card text-center">
                    <div class="card-header">
                        Fotoğraflar
                    </div>
                    <div class="card-body p-0">
                        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                @foreach ($publication->getMedia('images') as $image)
                                <li data-target="#carouselExampleIndicators" data-slide-to="{{ $loop->index }}" @if($loop->first) class="active" @endif></li>
                                @endforeach
                            </ol>
                            <div class="carousel-inner">
                                @foreach ($publication->getMedia('images') as $image)
                                <div class="carousel-item @if($loop->first) active @endif">
                                    <img src="{{ asset($image->getFullUrl()) }}" class="d-block w-100" alt="">
                                </div>
                                @endforeach
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="card text-center">
                    <div class="card-header bg-primary text-white">
                        {{ $publication->name }}
                    </div>
                    <div class="card-body">
                        {!! $publication->content !!}
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card text-center">
                    <div class="card-header bg-success text-white">
                        {{ $publication->seller->name }}
                    </div>
                    <div class="card-body">
                        <p>
                            <img src="https://www.gravatar.com/avatar/{{ md5($publication->seller->email) }}.png" class="img-thumbnail rounded-circle" />
                        </p>
                        <p>Üyelik tarihi: {{ $publication->seller->created_at->format("d.m.Y") }}</p>
                        <p class="m-0">İlan Sayısı: {{ $publication->seller->publications()->count() }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-md-12">
                <ul class="nav nav-tabs" id="tabs" role="tablist">
                    <li class="nav-item" role="presentation">
                        <a class="nav-link active" id="tech-tab" data-toggle="tab" href="#tech" role="tab" aria-controls="tech" aria-selected="true">Teknik Özellikler</a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" id="expertise-tab" data-toggle="tab" href="#expertise" role="tab" aria-controls="expertise" aria-selected="false">Expertiz Raporu</a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" id="location-tab" data-toggle="tab" href="#location" role="tab" aria-controls="location" aria-selected="false">Lokasyon</a>
                    </li>
                </ul>
                <div class="tab-content" id="tabContents">
                    <div class="tab-pane fade show active p-4" style="border: 1px solid #dee2e6; border-top: 0;" id="tech" role="tabpanel" aria-labelledby="tech-tab">
                        {!! $publication->tech !!}
                    </div>
                    <div class="tab-pane fade p-4" style="border: 1px solid #dee2e6; border-top: 0;" id="expertise" role="tabpanel" aria-labelledby="expertise-tab">
                        <a href="{{ route('publication.expertise', $publication->slug) }}" class="btn btn-primary">Expertiz Raporunu İndir</a>
                    </div>
                    <div class="tab-pane fade p-4" style="border: 1px solid #dee2e6; border-top: 0;" id="location" role="tabpanel" aria-labelledby="location-tab">
                        <p>{{ $publication->address->address }}</p>
                        <iframe
                            style="width: 100%; height: 450px; border: 0;"
                            frameborder="0"
                            src="https://www.google.com/maps/embed/v1/place?key={{ config('services.google_maps.api') }}&q={{ $publication->address->latitude }},{{ $publication->address->longitude }}" allowfullscreen>
                        </iframe>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
