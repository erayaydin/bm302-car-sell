@extends('layout.page')

@section('page')
    <main role="main" class="container mb-5 mt-5">
        <div class="row justify-content-center">
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header bg-success text-white text-center">
                        Yeni İlan Ekle
                    </div>
                    <div class="card-body">
                        @if ($errors->count())
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $error)
                                    <p class="mb-0">{{ $error }}</p>
                                @endforeach
                            </div>
                        @endif
                        <form method="post" action="{{ route('publication.store') }}" enctype="multipart/form-data">
                            {!! csrf_field() !!}
                            <div class="form-group">
                                <label for="name">İlan Başlığı</label>
                                <input type="text" name="name" id="name" value="{{ old('name') }}" placeholder="İlan Başlığı" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="category_id">Kategori</label>
                                <select id="category_id" name="category_id" class="form-control">
                                    @foreach (\App\Models\Category::all() as $item)
                                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="address_id">Adres</label>
                                <select id="address_id" name="address_id" class="form-control">
                                    @foreach (auth()->user()->addresses as $item)
                                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="content">İlan İçeriği</label>
                                <textarea name="content" id="content" class="form-control editor" rows="5" placeholder="İlan içeriğini giriniz..."></textarea>
                            </div>
                            <div class="form-group">
                                <label for="content">Teknik Detaylar</label>
                                <textarea name="tech" id="tech" class="form-control editor" rows="5" placeholder="İlan teknik detayları giriniz..."></textarea>
                            </div>
                            <div class="form-group">
                                <label for="images">Fotoğraflar</label>
                                <input type="file" name="images[]" id="images" multiple class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="brand">İlan Marka / Model</label>
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="text" name="brand" id="brand" value="{{ old('brand') }}" placeholder="Marka" class="form-control">
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" name="model" id="model" value="{{ old('model') }}" placeholder="Model" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="age">Araç Yaşı</label>
                                <input type="number" name="age" id="age" min="1" value="{{ old('age') ?: 1 }}" placeholder="Aracın Yaşı" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="made_in">Üretildiği Yer</label>
                                <input type="text" name="made_in" id="made_in" value="{{ old('made_in') }}" placeholder="Aracın Üretildiği Yer" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="expertise">Expertiz Raporu</label>
                                <input type="file" name="expertise" id="expertise" class="form-control">
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-success">İlanı Yayınla</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
