<!doctype html>
<html lang="@yield('page.lang', 'en')">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="@yield('page.description', config('app.name'))">
    <meta name="author" content="Eray Aydın <er@yayd.in>">
    <title>@yield('page.title', config('app.name'))</title>

    <link rel="canonical" href="@yield('page.canonical')">

    <!-- Main Stylesheets -->
    @stack('style.main')

    <!-- Plugin Stylesheets -->
    @stack('style.plugins')

    <!-- Page Stylesheets -->
    @stack('style.page')
</head>
<body class="@yield('body.class', 'app')">

@yield('body')

@stack('script.main')
<script src="https://cdn.tiny.cloud/1/haak987xpfyx86h57o27jumthkh2kakeqnyd4mxvfjn2b7fk/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>tinymce.init({selector:'.editor'});</script>
<script type="text/javascript" src='http://maps.google.com/maps/api/js?key=AIzaSyBAu6gFz9SsMSdB_1cp2k82XjVNLz10-9g&sensor=false&libraries=places'></script>
<script src="{{ asset('locationpicker/locationpicker.jquery.js') }}"></script>
@stack('script.plugins')
@stack('script.page')
</body>
</html>
