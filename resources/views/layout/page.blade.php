@extends('layout.master')

@push('style.main')
    <!-- Bootstrap core CSS -->
    <link href="{{ asset('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:700,900" rel="stylesheet">

    <style>
        .app-header-logo {
            font-family: "Roboto", Georgia, "Times New Roman", serif;
            font-size: 2.25rem;
        }

        .app-header-logo:hover {
            text-decoration: none;
        }

        .nav-scroller {
            position: relative;
            z-index: 2;
            height: 2.75rem;
            overflow-y: hidden;
        }

        .nav-scroller .nav {
            display: -ms-flexbox;
            display: flex;
            -ms-flex-wrap: nowrap;
            flex-wrap: nowrap;
            padding-bottom: 1rem;
            margin-top: -1px;
            overflow-x: auto;
            text-align: center;
            white-space: nowrap;
            -webkit-overflow-scrolling: touch;
        }

        .nav-scroller .nav-link {
            padding-top: .75rem;
            padding-bottom: .75rem;
            font-size: .875rem;
        }

        .app-footer {
            padding: 2.5rem 0;
            color: #999;
            text-align: center;
            background-color: #f9f9f9;
            border-top: .05rem solid #e5e5e5;
        }
        .app-footer p:last-child {
            margin-bottom: 0;
        }

        h1, h2, h3, h4, h5, h6 {
            font-family: "Roboto", Georgia, "Times New Roman", serif;
        }

    </style>
@endpush

@push('script.main')
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
@endpush

@section('body')
    <div class="container">
        @include('partial.header')

        @include('partial.nav')
    </div>

    @yield('page')

    @include('partial.footer')
@endsection
