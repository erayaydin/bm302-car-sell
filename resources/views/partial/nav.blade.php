<div class="nav-scroller py-1 mb-2">
    <nav class="nav d-flex justify-content-between">
        @foreach (\App\Models\Category::all() as $nav)
            <a class="nav-link p-2 @if(isset($category) && $category->id == $nav->id) text-primary @else text-muted @endif" href="{{ route('category.show', $nav->slug) }}">{{ $nav->name }}</a>
        @endforeach
    </nav>
</div>
