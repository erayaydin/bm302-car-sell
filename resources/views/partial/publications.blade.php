@if($publications->count() > 0)
    @foreach ($publications->chunk(4) as $chunk)
        <div class="row">
            @foreach ($chunk as $publication)
                <div class="col-md-3">
                    <div class="no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
                        <div>
                            <img src="http://placehold.it/800x600" style="width: 100%" />
                        </div>
                        <div class="p-4 d-flex flex-column position-static">
                            <p class="m-0">
                                <strong class="d-inline-block mb-2 text-primary">{{ $publication->category->name }}</strong>
                                <strong class="d-inline-block mb-2 text-success float-right">{{ $publication->seller->name }}</strong>
                            </p>
                            <h3 class="mb-0">{{ $publication->name }}</h3>
                            <div class="mb-1 text-muted">{{ $publication->created_at->locale('tr')->diffForHumans() }}</div>
                            <p class="card-text mb-auto">{{ $publication->description }}</p>
                            <a href="{{ route('publication.show', $publication->slug) }}" class="stretched-link btn btn-primary mt-3">Detaylar</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @endforeach

    @if (isset($paginated) && $paginated)
        {!! $publications->links() !!}
    @endif
@else
    <div class="alert alert-danger">
        <h3>İlan bulunamadı!</h3>
    </div>
@endif
