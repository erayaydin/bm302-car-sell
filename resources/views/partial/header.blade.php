<header class="app-header py-3">
    <div class="row flex-nowrap justify-content-between align-items-center">
        <div class="col-4 pt-1">
            <a class="btn btn-sm btn-secondary" href="{{ route('publication.index') }}">İlanlar</a>
            <a class="btn btn-sm btn-success" href="{{ route('publication.create') }}">İlan Ekle</a>
        </div>
        <div class="col-4 text-center">
            <a class="app-header-logo text-dark" href="{{ route('home.index') }}">{{ config('app.name') }}</a>
        </div>
        <div class="col-4 d-flex justify-content-end align-items-center">
            <a class="text-muted" href="#" aria-label="Search">
                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="mx-3" role="img" viewBox="0 0 24 24" focusable="false"><title>Arama</title><circle cx="10.5" cy="10.5" r="7.5"/><path d="M21 21l-5.2-5.2"/></svg>
            </a>
            @if(auth()->check())
                <a class="btn btn-sm btn-success" href="{{ route('address.index') }}">Adreslerim</a> &nbsp;
                <a class="btn btn-sm btn-primary" href="#">Hesabım</a> &nbsp;
                <a class="btn btn-sm btn-danger" href="{{ route('auth.logout') }}">Çıkış Yap</a>
            @else
                <a class="btn btn-sm btn-primary" href="{{ route('auth.login') }}">Giriş Yap</a> &nbsp;
                <a class="btn btn-sm btn-success" href="{{ route('auth.register') }}">Kayıt Ol</a>
            @endif
        </div>
    </div>
</header>
