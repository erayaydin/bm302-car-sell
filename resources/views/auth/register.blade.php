@extends('layout.page')

@push('script.page')
    <script>
        function loadForm() {
            var type = $("input[name='type']:checked").val();

            var memberFields = $("#memberFields");
            var sellerFields = $("#sellerFields");

            memberFields.hide();
            sellerFields.hide();

            if (type === "seller") {
                $("#nameLabel").html("Mağaza Adı");
                $("#nameTxt").attr('placeholder', "Mağaza Adı");

                sellerFields.show();
            } else {
                $("#nameLabel").html("Ad Soyad");
                $("#nameTxt").attr('placeholder', "Ad Soyad");

                memberFields.show();
            }
        }

        loadForm();
        $("input[name='type']").change(function() {
            loadForm();
        });
    </script>
@endpush

@section('page')
    <main role="main" class="container mb-5 mt-5">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header bg-success text-white text-center">
                        Kayıt Ol
                    </div>
                    <div class="card-body">
                        @if ($errors->count())
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $error)
                                    <p class="mb-0">{{ $error }}</p>
                                @endforeach
                            </div>
                        @endif
                        <form method="post" action="{{ route('auth.do-register') }}">
                            {!! csrf_field() !!}
                            <div class="form-group">
                                <label for="email">Üyelik Türü</label>
                                <div>
                                    <label>
                                        <input type="radio" name="type" @if(old('type') == "member" || !old('type')) checked @endif value="member" />
                                        Bireysel Üyelik
                                    </label>
                                </div>
                                <div>
                                    <label>
                                        <input type="radio" name="type" @if(old('type') == "seller") checked @endif value="seller" />
                                        Kurumsal Üyelik
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name" id="nameLabel">Ad Soyad</label>
                                <input type="text" name="name" id="nameTxt" value="{{ old('name') }}" placeholder="Ad Soyad" class="form-control">
                            </div>
                            <div id="memberFields">
                                <div class="form-group">
                                    <label for="name">Kimlik Numarası</label>
                                    <input type="text" name="identity" value="{{ old('identity') }}" placeholder="T.C. Kimlik Numarası" class="form-control">
                                </div>
                            </div>
                            <div id="sellerFields" style="display: none;">
                                <div class="form-group">
                                    <label for="name">Vergi Dairesi / No</label>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="text" name="tax_place" value="{{ old('tax_place') }}" placeholder="Vergi Dairesi" class="form-control">
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" name="tax_id" value="{{ old('tax_id') }}" placeholder="Vergi Numarası" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email">E-Posta Adresi</label>
                                <input type="email" name="email" value="{{ old('email') }}" placeholder="E-Posta Adresi" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="password">Parola</label>
                                <input type="password" name="password" placeholder="Parola" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="password_confirmation">Parola Onayı</label>
                                <input type="password" name="password_confirmation" placeholder="Parola Onayı" class="form-control">
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-success">Kayıt Ol</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
