@extends('layout.page')

@section('page')
    <main role="main" class="container mb-5 mt-5">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header bg-primary text-white text-center">
                        Giriş Yap
                    </div>
                    <div class="card-body">
                        @if ($errors->count())
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $error)
                                    <p class="mb-0">{{ $error }}</p>
                                @endforeach
                            </div>
                        @endif
                        <form method="post" action="{{ route('auth.do-login') }}">
                            {!! csrf_field() !!}
                            <div class="form-group">
                                <label for="email">E-Posta Adresi</label>
                                <input type="email" name="email" value="{{ old('email') }}" placeholder="E-Posta Adresi" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="password">Parola</label>
                                <input type="password" name="password" placeholder="Parola" class="form-control">
                            </div>
                            <label>
                                <input type="checkbox" name="remember" value="1" />
                                Beni Hatırla
                            </label>
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary">Giriş Yap</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
