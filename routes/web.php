<?php

use App\Models\Category;
use App\Models\Publication;
use Illuminate\Routing\Router;

/** @var Router $route */
/** @noinspection PhpUnhandledExceptionInspection */
$route = app()->make('router');

$route->bind('category', function($value){
    return Category::query()->where('slug', $value)->first();
});

$route->bind('publication', function($value) {
    return Publication::query()->where('slug', $value)->first();
});

/**
 * Auth only routes
 */
$route->group(['middleware' => ['auth']], function(Router $route) {

    $route->group(['as' => 'auth.', 'prefix' => 'auth'], function(Router $route) {
        $route->get('logout', 'AuthController@logout')->name('logout');
    });

    $route->resource('address', 'AddressController')->except(['show']);

    $route->group(['as' => 'publication.', 'prefix' => 'publication'], function(Router $route) {
        $route->get('create', 'PublicationController@create')->name('create');
        $route->post('/', 'PublicationController@store')->name('store');
    });

});

/**
 * Guest only routes
 */
$route->group(['middleware' => ['guest']], function(Router $route) {

    $route->group(['as' => 'auth.', 'prefix' => 'auth'], function(Router $route){
        $route->get('login', 'AuthController@login')->name('login');
        $route->post('login', 'AuthController@doLogin')->name('do-login');

        $route->get('register', 'AuthController@register')->name('register');
        $route->post('register', 'AuthController@doRegister')->name('do-register');
    });

});

/**
 * Public Routes
 */
$route->get('/', 'HomeController@index')->name('home.index');

$route->group(['as' => 'category.', 'prefix' => 'category'], function(Router $route) {
    $route->get('/', 'CategoryController@index')->name('index');
    $route->get('{category}', 'CategoryController@show')->name('show');
});

$route->group(['as' => 'publication.', 'prefix' => 'publication'], function(Router $route) {
    $route->get('/', 'PublicationController@index')->name('index');
    $route->get('{publication}', 'PublicationController@show')->name('show');
    $route->get('{publication}/expertise', 'PublicationController@expertise')->name('expertise');
});
