<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Category extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $fillable = [
        'name', 'slug',
    ];

    /**
     * Retrieve publications of the category
     *
     * @return HasMany
     */
    public function publications(): HasMany
    {
        return $this->hasMany(Publication::class);
    }
}
