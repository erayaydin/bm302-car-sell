<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PublicationCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => "required",
            'category_id' => "required|exists:categories,id",
            'address_id' => "required|exists:addresses,id",
            "content" => "required",
            "tech" => "required",
            "brand" => "required",
            "model" => "required",
            "age" => "required|numeric",
            "made_in" => "required",
        ];

        return $rules;
    }
}
