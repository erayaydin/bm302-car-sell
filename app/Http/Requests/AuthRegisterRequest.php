<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AuthRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => "required|in:member,seller",
            'name' => "required",
            "identity" => "required_if:type,member|nullable|size:11",
            "tax_place" => "required_if:type,seller",
            "tax_id" => "required_if:type,seller",
            "email" => "required|email|unique:users",
            "password" => "required|min:8|confirmed",
        ];
    }
}
