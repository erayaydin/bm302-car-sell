<?php

namespace App\Http\Controllers;

use App\Models\Publication;

class HomeController extends Controller
{
    public function index()
    {
        $publications = Publication::query()->latest()->take(20)->get();

        return view('home.index', [
            'publications' => $publications,
        ]);
    }
}
