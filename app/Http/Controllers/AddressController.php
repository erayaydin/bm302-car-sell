<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddressCreateRequest;
use App\Http\Requests\AddressEditRequest;
use App\Models\Address;
use App\Models\User;

class AddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /** @var User $user */
        $user = auth()->user();
        $addresses = $user->addresses()->latest()->paginate(20);

        return view('address.index', [
            'addresses' => $addresses,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('address.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AddressCreateRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddressCreateRequest $request)
    {
        $address = new Address($request->only('name', 'address', 'latitude', 'longitude'));
        $address->user()->associate(auth()->user());
        $address->save();

        return redirect()->route('address.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Address $address
     * @return \Illuminate\Http\Response
     */
    public function edit(Address $address)
    {
        return view('address.edit', [
            'address' => $address,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param AddressEditRequest $request
     * @param Address $address
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(AddressEditRequest $request, Address $address)
    {
        $address->update($request->only('name', 'address', 'latitude', 'longitude'));

        return redirect()->route('address.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Address $address
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Address $address)
    {
        $address->delete();

        return redirect()->route('address.index');
    }
}
