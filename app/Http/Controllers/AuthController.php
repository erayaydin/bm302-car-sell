<?php

namespace App\Http\Controllers;

use App\Http\Requests\AuthLoginRequest;
use App\Http\Requests\AuthRegisterRequest;
use App\Models\User;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function login()
    {
        return view('auth.login');
    }

    public function doLogin(AuthLoginRequest $request)
    {
        if (auth()->attempt($request->only('email', 'password'), $request->get('remember')))
            return redirect()->route('home.index');
        return redirect()->route('auth.login')->withInput()->withErrors(['email' => trans('auth.failed')]);
    }

    public function register()
    {
        return view('auth.register');
    }

    public function doRegister(AuthRegisterRequest $request)
    {
        $newUser = $request->only('name', 'email', 'identity', 'tax_id', 'tax_place');
        $newUser['password'] = bcrypt($request->get('password'));

        if ($request->get('type') == "seller")
            $newUser['is_company'] = true;

        User::query()->create($newUser);

        return redirect()->route('auth.login')->withInput(['email']);
    }

    public function logout()
    {
        auth()->logout();

        return redirect()->route('home.index');
    }
}
