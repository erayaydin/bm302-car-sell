<?php

namespace App\Http\Controllers;

use App\Http\Requests\PublicationCreateRequest;
use App\Models\Publication;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;

class PublicationController extends Controller
{
    public function index()
    {
        $publications = Publication::query()->latest()->paginate(20);

        return view('publication.index', [
            'publications' => $publications,
        ]);
    }

    public function show(Publication $publication)
    {
        return view('publication.show', [
            'publication' => $publication,
            'category' => $publication->category,
        ]);
    }

    public function create()
    {
        return view('publication.create');
    }

    public function store(PublicationCreateRequest $request)
    {
        $publication = new Publication($request->only(
            "name", "content", "tech", "brand", "model", "age", "made_in"
        ));

        $publication->slug = Str::slug($request->get('name'));
        $publication->category()->associate($request->get('category_id'));
        $publication->address()->associate($request->get('address_id'));
        $publication->seller()->associate(auth()->user()->id);

        $publication->save();

        foreach ($request->file('images') as $image)
            $publication->addMedia($image)->toMediaCollection('images');
        $publication->addMedia($request->file('expertise'))->toMediaCollection('expertise');

        return redirect()->route('publication.show', $publication->slug);
    }

    public function expertise(Publication $publication)
    {
        return response()->download($publication->getFirstMediaPath('expertise'));
    }
}
